def buildJar() {
    echo "building stage ..."
    sh 'mvn package'
}

def buildPushImage() {
    echo "building image"
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]){
        sh 'docker build -t bahaem/bahaemoutaoukil:jma-1.1 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push bahaem/bahaemoutaoukil:jma-1.1'
    }
}

def deployApp(){
    echo "deploying ..."
}

return this
